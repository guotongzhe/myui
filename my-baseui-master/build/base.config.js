import { defineConfig } from 'vite';
// 将 vue 文件字符串编译成函数组件返回给前端
import Vue from '@vitejs/plugin-vue';
// 解析 Markdown 文件并把它变成 Vue 文件
import Markdown from 'vite-plugin-md';
import { resolve } from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    Vue({
      include: [/\.vue$/, /\.md$/],
    }),
    Markdown()
  ],
  resolve: {
    alias: {
      "@": resolve(__dirname, "../src"),
      "~": resolve(__dirname, "../packages")
    }
  },
})
